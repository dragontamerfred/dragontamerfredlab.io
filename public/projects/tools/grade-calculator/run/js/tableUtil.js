function addRow(correct, incorrect, grade) {
    var table = document.getElementById('gradeTable').getElementsByTagName('tbody')[0]
    table.innerHTML += '<tr><td>' + correct + '</td><td>' + incorrect + '</td><td>' + grade + '</td></tr>'
}

function clearTable() {
    document.getElementById('gradeTable').getElementsByTagName('tbody')[0].innerHTML = ""
}