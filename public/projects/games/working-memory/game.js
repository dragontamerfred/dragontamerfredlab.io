let state = 0
let currentNumber = 2
let startTime
let firstClickTime
let endTime

let gameMap;

function setup() {
    createCanvas(513, 385)
    reset()
  }
  
function reset() {
    gameMap = new GameMap();
    currentNumber = 2;
    state = 0;
    for (let i=1;i<=9;i++) {
        let x = int(random(7))
        let y = int(random(5))
        while (gameMap.getTileAtPos(x, y) != null) {
            x = int(random(7))
            y = int(random(5))
        }
        gameMap.addTile(new Tile(x, y, i), i)
    }
    startTime = Date.now()
    print(startTime)
}

function draw() {
    background(0);
    fill(255)
    if (state === 0) {
        for (let i in gameMap.tiles) {
            let tile = gameMap.tiles[i]
            textSize(30)
            textAlign(CENTER, CENTER)
            text(i, tile.x*64+32, tile.y*64+32)
        }
    } else if (state === 1) {
        for (let i in gameMap.tiles) {
            if (i >= currentNumber) {
                let tile = gameMap.tiles[i]
                fill(50)
                noStroke()
                rect(tile.x*64, tile.y*64, 64, 64)
            }
        }
    } else if (state === 3) {
        fill(200)
        noStroke()
        textSize(30)
        textAlign(CENTER, CENTER)
        text("You win!\nLearning time: " + ((firstClickTime-startTime)/1000).toFixed(2) + "s\nExecution time: " + ((endTime-firstClickTime)/1000).toFixed(2) + "s\nClick anywhere to restart.", 513/2, 385/2)
    } else if (state === 99) {
        for (let i in gameMap.tiles) {
            if (i >= currentNumber) {
                let tile = gameMap.tiles[i]
                fill(255, 100)
                stroke(255, 50)
                textSize(30)
                textAlign(CENTER, CENTER)
                text(i, tile.x*64+32, tile.y*64+32)
            }
        }

        fill(200)
        noStroke()
        textSize(30)
        textAlign(CENTER, CENTER)
        text("You lost.\nLearning time: " + ((firstClickTime-startTime)/1000).toFixed(2) + "s\nClick anywhere to try again.", 513/2, 385/2)
    }
}

function mouseClicked() {
    if (state === 0) {
        let x = (mouseX - mouseX % 64) / 64;
        let y = (mouseY - mouseY % 64) / 64;
        let tile = gameMap.getTileAtPos(x, y);
        if (tile != null) {
            if (tile.num === 1) {
                state = 1;
                firstClickTime = Date.now()
                print(firstClickTime)
            }
        }
    } else if (state === 1) {
        let x = (mouseX - mouseX % 64) / 64;
        let y = (mouseY - mouseY % 64) / 64;
        let tile = gameMap.getTileAtPos(x, y);
        if (tile != null) {
            if (tile.num === currentNumber) {
                currentNumber++;
                if (currentNumber === 10) {
                    state = 3
                    endTime = Date.now()
                    print(endTime)
                }
            } else {
                state = 99;
            }
        }
    } else if (state === 3) {
        reset();
    } else if (state === 99) {
        reset();
    }
 }

class GameMap {
    constructor() {
        this.tiles = {};
    }
    
    getTileAtPos(x, y) {
        for (let i in this.tiles) {
            let tile = this.tiles[i]
            if (tile.x === x && tile.y === y) {
                return tile;
            }
        }
        return null;
    }

    addTile(tile, num) {
        this.tiles[num] = tile
    }

}

class Tile {
    constructor(x, y, num) {
        this.x = x;
        this.y = y;
        this.num = num;
    }
}
